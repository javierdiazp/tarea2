package sh4j.model.format;

import sh4j.model.highlight.SDummy;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;
import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;

/**
 * Entrega formato de tipo HTML a un texto que representa codigo.
 * Considera cualquier estilo de texto escrito en HTML incluyendo
 * color, fuente, etc.
 */
public class SHTMLFormatter implements SFormatter{

  /**
   * Describe un contenedor de texto.
   */
  private final StringBuffer buffer;

  /**
   * Describe el nivel de indentacion.
   */
  private int level;
  
  /**
   * Describe el estilo del texto.
   */
  private final SStyle style;
  
  /**
   * Describe los tipos de palabras destacadas.
   */
  private final SHighlighter[] highlighters;
  
  /**
   * Describe las lineas de codigo.
   */
  private int lines;
  
  /**
   * Mostrar o no las lineas de codigo.
   */
  private boolean linesOfCode = true;
  
  /**
   * Define un contenedor de texto con palabras 
   * que son destacadas segun un estilo.
   * @param style estilo de texto con formato especifico.
   * @param hs tipos de palabras que son destacadas.
   */
  public SHTMLFormatter(SStyle style, SHighlighter... hs) {
    this.style = style;
    highlighters = hs;
    lines = 1;
    buffer = new StringBuffer();
  }

  private SHighlighter lookup(String text) {
    for (SHighlighter h: highlighters) {
      if (h.needsHighLight(text)) {
        return h;
      }
    }
    return new SDummy();
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledWord(java.lang.String)
   */
  @Override
  public void styledWord(String word) {
    buffer.append(lookup(word).highlight(word, style));
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledChar(char)
   */
  @Override
  public void styledChar(char charac) {
    buffer.append(lookup(charac + "").highlight(charac + "", style));
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledSpace()
   */
  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledCR()
   */
  @Override
  public void styledCR() {
    String lineNumber = linesOfCode ? tag("span", String.valueOf("  " + lines + " "), 
        "background:#f1f0f0;") : "";
    buffer.append("\n" + lineNumber);
    lines++;
    indent();
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledBlock(sh4j.parser.model.SBlock)
   */
  @Override
  public void styledBlock(SBlock block) {
    level++;
    for (SText text: block.texts()) {
      text.export(this);
    }
    level--;
  }

  /**
   * Agrega indentacion.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#formattedText()
   */
  @Override
  public String formattedText() {
    return style.formatBody(buffer.toString());
  }
  
  /**
   * Retorna codigo html que da formato a un texto.
   * @param name nombre de tag
   * @param content contenido del tag
   * @param style estilo del tag
   * @return string que da formato name a content, con estilo style
   */
  public static String tag(String name,String content,String style) {
    return "<" + name + " style='" + style + "'>" + content + "</" + name + ">";
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#getLines()
   */
  @Override
  public int getLines() {
    return lines;
  }
}

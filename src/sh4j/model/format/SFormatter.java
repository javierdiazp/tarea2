package sh4j.model.format;

import sh4j.parser.model.SBlock;

/**
 * Define los elementos que poseen formato
 * dentro de un codigo.
 * @author Javier
 */
public interface SFormatter {
  
  /**
   * Agrega un texto con formato.
   * @param word texto sin formato.
   */
  public void styledWord(String word);

  /**
   * Agrega un caracter con formato.
   * @param charac caracter sin formato.
   */
  public void styledChar(char charac);

  /**
   * Agrega un espacio.
   */
  public void styledSpace();

  /**
   * Agrega un salto de linea.
   */
  public void styledCR();

  /**
   * Agrega un bloque de codigo con 
   * su indentacion correspondiente.
   * @param block bloque de codigo entre llaves, sin formato.
   */
  public void styledBlock(SBlock block);

  /**
   * @return texto almacenado.
   */
  public String formattedText();

  /**
   * Retorna el numero de lineas del codigo.
   * @return numero de lineas de codigo
   */
  public int getLines();
}
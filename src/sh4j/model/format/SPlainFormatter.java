package sh4j.model.format;

import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;

/**
 * Entrega formato plano a un texto que representa codigo.
 * Solo considera espaciado entre las palabras.
 */
public class SPlainFormatter implements SFormatter{

  /**
   * Describe un contenedor de texto.
   */
  private final StringBuffer buffer;
  
  /**
   * Describe el nivel de indentacion.
   */
  private int level;
  
  /**
   * Describe las lineas de codigo.
   */
  private int lines;

  /**
   * Define un contenedor de texto con formato.
   */
  public SPlainFormatter() {
    buffer = new StringBuffer();
  }
  
  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledWord(java.lang.String)
   */
  @Override
  public void styledWord(String word) {
    buffer.append(word);
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledChar(char)
   */
  @Override
  public void styledChar(char charac) {
    buffer.append(charac);
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledSpace()
   */
  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledCR()
   */
  @Override
  public void styledCR() {
    buffer.append('\n');
    lines++;
    indent();
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledBlock(sh4j.parser.model.SBlock)
   */
  @Override
  public void styledBlock(SBlock block) {
    level++;
    for (SText text: block.texts()) {
      text.export(this);
    }
    level--;
  }

  /**
   * Agrega indentacion.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#formattedText()
   */
  @Override
  public String formattedText() {
    return buffer.toString();
  }

  /** (non-Javadoc)
   * @see sh4j.model.format.SFormatter#getLines()
   */
  @Override
  public int getLines() {
    return lines;
  }
}
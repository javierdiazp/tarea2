package sh4j.model.style;

/**
 * Define la estructura de las clases que representan
 * los estilos que personalizan un texto.
 * @author Javier
 */
public interface SStyle {

  /**
   * Retorna el nombre del estilo.
   * @return el nombre del estilo
   */
  public String toString();

  /**
   * Destaca las palabras de tipo ClassName segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatClassName(String text);

  /**
   * Destaca las palabras de tipo CurlyBracket segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatCurlyBracket(String text);

  /**
   * Destaca las palabras de tipo KeyWord segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatKeyWord(String text);

  /**
   * Destaca las palabras de tipo PseudoVariable segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatPseudoVariable(String text);

  /**
   * Destaca las palabras de tipo SemiColon segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatSemiColon(String text);

  /**
   * Destaca las palabras de tipo String segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatString(String text);

  /**
   * Destaca las palabras de tipo MainClass segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatMainClass(String text);

  /**
   * Destaca las palabras de tipo Modifier segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatModifier(String text);

  /**
   * Destaca el cuerpo del codigo segun las reglas del estilo.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return el codigo HTML del texto destacado
   */
  public String formatBody(String text);

}
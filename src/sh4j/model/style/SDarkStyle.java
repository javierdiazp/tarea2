package sh4j.model.style;

import static sh4j.model.format.SHTMLFormatter.tag;

/**
 * Provee el conjunto de reglas del estilo dark.
 * @author Javier
 */
public class SDarkStyle implements SStyle{

  /** (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "dark";
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatClassName(java.lang.String)
   */
  @Override
  public String formatClassName(String text) {
    return text;
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatCurlyBracket(java.lang.String)
   */
  @Override
  public String formatCurlyBracket(String text) {
    return tag("span",text, "color:#b060b0; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatKeyWord(java.lang.String)
   */
  @Override
  public String formatKeyWord(String text) {
    return tag("span",text, "color:#bb7977; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatPseudoVariable(java.lang.String)
   */
  @Override
  public String formatPseudoVariable(String text) {
    return tag("span",text,"color:#e66170; font-weight:bold; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatSemiColon(java.lang.String)
   */
  @Override
  public String formatSemiColon(String text) {
    return tag("span",text,"color:#b060b0; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatString(java.lang.String)
   */
  @Override
  public String formatString(String text) {
    return tag("span",text,"color:#00c4c4; ");       
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatMainClass(java.lang.String)
   */
  @Override
  public String formatMainClass(String text) {
    return tag("span",text,"color:#bb7977; font-weight:bold; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatBody(java.lang.String)
   */
  @Override
  public String formatBody(String text) {
    return tag("pre",text,"color:#d1d1d1;background:#000000;");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatModifier(java.lang.String)
   */
  @Override
  public String formatModifier(String text) {
    return tag("span",text,"color:#e66170; font-weight:bold; ");
  }

}

package sh4j.model.style;

import static sh4j.model.format.SHTMLFormatter.tag;

/**
 * Provee el conjunto de reglas del estilo bred.
 * @author Javier
 */
public class SBredStyle  implements SStyle{

  /** (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "bred";
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatClassName(java.lang.String)
   */
  @Override
  public String formatClassName(String text) {
    return text;
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatCurlyBracket(java.lang.String)
   */
  @Override
  public String formatCurlyBracket(String text) {
    return tag("span",text, "color:#806030; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatKeyWord(java.lang.String)
   */
  @Override
  public String formatKeyWord(String text) {
    return tag("span",text, "color:#800040; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatPseudoVariable(java.lang.String)
   */
  @Override
  public String formatPseudoVariable(String text) {
    return tag("span",text, "color:#400000; font-weight:bold;  ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatSemiColon(java.lang.String)
   */
  @Override
  public String formatSemiColon(String text) {
    return tag("span",text, "color:#806030; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatString(java.lang.String)
   */
  @Override
  public String formatString(String text) {
    return tag("span",text, "color:#e60000; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatMainClass(java.lang.String)
   */
  @Override
  public String formatMainClass(String text) {
    return tag("span",text, "color:#800040; ");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatBody(java.lang.String)
   */
  @Override
  public String formatBody(String text) {
    return tag("pre",text, "color:#000000;background:#f1f0f0;");
  }

  /** (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatModifier(java.lang.String)
   */
  @Override
  public String formatModifier(String text) {
    return tag("span",text, "color:#400000; font-weight:bold; ");
  }
}
package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;

/**
 * Representacion de un elemento de Java.
 * @author Javier
 */
public interface SObject {
  
  /**
   * Retorna la direccion de la imagen del icono.
   * @return direccion de la imagen del icono
   */
  public String icon();
  
  /**
   * Retorna la fuente.
   * @return fuente del elemento
   */
  public Font font();
  
  /**
   * Retorna el color de fondo.
   * @return color de fondo del elemento
   */
  public Color background();
}

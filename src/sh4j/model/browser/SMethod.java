package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;

import sh4j.model.format.SPlainFormatter;
import sh4j.parser.model.SBlock;

import java.awt.Color;
import java.awt.Font;

/**
 * Representacion de los metodos de Java.
 * @author Javier
 */
public class SMethod implements SObject{

  /**
   * Declaracion del metodo.
   */
  private final MethodDeclaration declaration;

  /**
   * Cuerpo del metodo.
   */
  private final SBlock body;

  /**
   * Constructor de la clase.
   * @param node declaracion del metodo
   * @param body cuerpo del metodo
   */
  public SMethod(MethodDeclaration node,SBlock body) {
    declaration = node;
    this.body = body;
  }

  /**
   * Retorna el modificador del metodo.
   * @return modificador del metodo
   */
  public String modifier() {       
    for (Object obj : declaration.modifiers()) {
      if (obj instanceof Modifier) {
        Modifier modifier = (Modifier) obj;
        return modifier.getKeyword().toString();
      }
    }
    return "default";
  }

  /**
   * Retorna el nombre del metodo.
   * @return nombre del metodo
   */
  public String name() {
    return declaration.getName().getIdentifier();
  }

  /**
   * Retorna el cuerpo del metodo.
   * @return cuerpo del metodo
   */
  public SBlock body() {
    return body;
  }

  /** (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return name();
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#icon()
   */
  @Override
  public String icon() {
    return "./resources/" + this.modifier() + "_co.gif";
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#font()
   */
  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#background()
   */
  public Color background() {
    int lineNum = this.getLinesOfCode();
    if (lineNum > 50) {
      return Color.RED;
    }
    if (lineNum > 30) {
      return Color.YELLOW;
    }
    return Color.WHITE;
  }

  /**
   * Retorna el numero de lineas del metodo.
   * @return numero de lineas del metodo
   */
  public int getLinesOfCode() {
    SPlainFormatter format = new SPlainFormatter();
    body.export(format);
    return format.getLines();
  }
}

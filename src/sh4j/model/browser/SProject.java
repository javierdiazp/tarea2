package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * Representacion de los proyectos de Java.
 * @author Javier
 */
public class SProject implements SObject{

  /**
   * Lista de paquetes del proyecto.
   */
  private final List<SPackage> packages;

  /**
   * Constructor de SProject.
   */
  public SProject() {
    packages = new ArrayList<SPackage>();
  }

  /**
   * Agrega pack a la lista de paquetes.
   * @param pack paquete de Java
   */
  public void addPackage(SPackage pack) {
    packages.add(pack);
  }

  /**
   * Retorna los paquetes del proyecto.
   * @return lista de paquetes
   */
  public List<SPackage> packages() {
    return packages;
  }

  /**
   * Busca y retorna un paquete por nombre.
   * @param pkgName nombre del paquete
   * @return paquete llamado pkgName, si esta en la lista
   */
  public SPackage get(String pkgName) {
    for (SPackage pkg: packages) {
      if (pkg.toString().equals(pkgName)) {
        return pkg;
      }
    }
    return null;
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#icon()
   */
  @Override
  public String icon() {
    return null;
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#font()
   */
  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#background()
   */
  @Override
  public Color background() {
    return Color.WHITE;
  }
}
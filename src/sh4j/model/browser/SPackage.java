package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * Representacion de un paquete de Java.
 * @author Javier
 */
public class SPackage implements SObject{

  /**
   * Nombre del paquete.
   */
  private final String name;

  /**
   * Lista de clases del paquete.
   */
  private final List<SClass> classes;

  /**
   * Constructor de la clase.
   * @param name nombre del paquete
   */
  public SPackage(String name) {
    classes = new ArrayList<SClass>();
    this.name = name;
  }

  /**
   * Agrega cls a la lista de clases.
   * @param cls clase de Java
   */
  public void addClass(SClass cls) {
    classes.add(cls);
  }

  /**
   * Retorna las clases del paquete.
   * @return lista de clases del paquete
   */
  public List<SClass> classes() {
    return classes;
  }

  /** (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return name;
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#icon()
   */
  @Override
  public String icon() {
    String emptyIcon = "./resources/pack_empty_co.gif";
    String noemptyIcon = "./resources/package_mode.gif";
    boolean isEmpty = this.classes().isEmpty();
    return isEmpty ? emptyIcon : noemptyIcon;
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#font()
   */
  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#background()
   */
  @Override
  public Color background() {
    return Color.WHITE;
  }
}

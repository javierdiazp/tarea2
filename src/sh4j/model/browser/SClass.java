package sh4j.model.browser;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

/**
 * Representacion de las clases de Java.
 * @author Javier
 */
public class SClass implements SObject {

  /**
   * Declaracion de tipo.
   */
  private final TypeDeclaration declaration;

  /**
   * Lista de metodos de la clase.
   */
  private final List<SMethod> methods;
  
  /**
   * Nivel de indentacion por jerarquia.
   */
  private int level;

  /**
   * Constructor de clase.
   * @param td declaracion de tipo
   * @param ms lista de metodos
   */
  public SClass(TypeDeclaration td, List<SMethod> ms) {
    declaration = td;
    methods = ms;
    level = 0;
  }

  /**
   * Retorna la lista de metodos.
   * @return lista de metodos de la clase
   */
  public List<SMethod> methods() {
    return methods;
  }

  /**
   * Retorna el nombre de la clase
   * @return nombre de la clase.
   */
  public String className() {
    return indent() + declaration.getName().getIdentifier();
  }

  private String indent() {
    String indentation = "";
    for (int i = 0; i < level; i++) {
      indentation += "    ";
    }
    return indentation;
  }

  /**
   * Decide si la clase es una interfaz.
   * @return true si la clase es una interfaz, false si no
   */
  public boolean isInterface() {
    return declaration.isInterface();
  }

  /**
   * Retorna el nombre de la superclase.
   * @return nombre de la superclase
   */
  public String superClass() {
    if (declaration.getSuperclassType() == null) {
      return "Object";
    }
    return declaration.getSuperclassType().toString();
  }

  /** (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return className();
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#icon()
   */
  @Override
  public String icon() {
    String interfaceIcon = "./resources/int_obj.gif";
    String classIcon = "./resources/class_obj.gif";
    boolean isInterface = this.isInterface();
    return isInterface ? interfaceIcon : classIcon;
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#font()
   */
  @Override
  public Font font() {
    Font plain = new Font("Helvetica", Font.PLAIN, 12);
    Font italic = new Font("Helvetica", Font.ITALIC, 12);
    boolean isInterface = this.isInterface();
    return isInterface ? italic : plain;
  }

  /** (non-Javadoc)
   * @see sh4j.model.browser.SObject#background()
   */
  @Override
  public Color background() {
    return Color.WHITE;
  }
}
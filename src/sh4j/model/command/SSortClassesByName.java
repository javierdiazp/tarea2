package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Comandos para ordenar clases segun su nombre.
 * @author Javier
 */
public class SSortClassesByName extends SCommand{

  /** (non-Javadoc)
   * @see sh4j.model.command.SCommand#executeOn(sh4j.model.browser.SProject)
   */
  @Override
  public void executeOn(SProject project) {
    List<SPackage> packages = project.packages();
    for (SPackage pkg: packages) {
      Comparator<SClass> cmp = ( c1, c2) -> c1.className().compareTo(c2.className());
      List<SClass> classes = pkg.classes();
      Collections.sort(classes, cmp);
    }
  }

}
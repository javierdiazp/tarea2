package sh4j.model.command;

import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Comandos para ordenar paquetes segun su nombre.
 * @author Javier
 */
public class SSortPackagesByName extends SCommand{

  /** (non-Javadoc)
   * @see sh4j.model.command.SCommand#executeOn(sh4j.model.browser.SProject)
   */
  @Override
  public void executeOn(SProject project) {
    Comparator<SPackage> cmp = ( p1, p2) -> p1.toString().compareTo(p2.toString());
    List<SPackage> packages = project.packages();
    Collections.sort(packages, cmp);
  }


}
package sh4j.model.command;

import sh4j.model.browser.SProject;

/**
 * It represent a command that could be applied to a project.
 * @author juampi
 *
 */
public abstract class SCommand {
  
  /**
   * Ejecuta la accion del comando sobre el proyecto.
   * @param project proyecto de Java
   */
  public abstract void executeOn(SProject project);
  
  /**
   * Retorna el nombre del comando.
   * @return nombre del comando
   */
  public String name() {
    return this.getClass().getName();
  }
  
}
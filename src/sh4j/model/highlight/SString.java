package sh4j.model.highlight;

import sh4j.model.style.SStyle;

/**
 * Describe el conjunto de palabras de tipo String.
 * @author Javier
 */
public class SString implements SHighlighter{

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
   */
  @Override
  public boolean needsHighLight(String text) {
    if (text.length() < 2) {
      return false;
    }
    return text.charAt(0) == '"' && text.charAt(text.length() - 1) == '"';
  }

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String, sh4j.model.style.SStyle)
   */
  @Override
  public String highlight(String text, SStyle style) {
    return style.formatString(text);
  }

}

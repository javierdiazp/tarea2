package sh4j.model.highlight;

import sh4j.model.style.SStyle;

import java.util.regex.Pattern;

/**
 * Describe el conjunto de palabras de tipo ClassName.
 */ 
public class SClassName implements SHighlighter {

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
   */
  @Override
  public boolean needsHighLight(String text) {
    return Pattern.compile("[A-Z_$]+[a-zA-Z0-9_$]*").matcher(text).matches();
  }

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String, sh4j.model.style.SStyle)
   */
  @Override
  public String highlight(String text,SStyle style) {
    return style.formatClassName(text);
  }
}
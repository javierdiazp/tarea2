package sh4j.model.highlight;

import sh4j.model.style.SStyle;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Describe el conjunto de palabras de tipo KeyWord.
 * @author Javier
 */
public class SKeyWord implements SHighlighter{

  /**
   * Conjunto de keywords de Java.
   */
  private static final Set<String> javaKeywords = new HashSet<String>(Arrays.asList(
      "abstract",     "assert",        "boolean",      "break",           "byte",
      "case",         "catch",         "char",         "class",           "const",
      "continue",     "default",       "do",           "double",          "else",
      "enum",         "extends",       "false",        "final",           "finally",
      "float",        "for",           "goto",         "if",              "implements",
      "import",       "instanceof",    "int",          "interface",       "long",
      "native",       "new",           "null",         "package",         
      "return",       "short",           "static",
      "strictfp",     "super",         "switch",       "synchronized",    "this",
      "throw",        "throws",        "transient",    "true",            "try",
      "void",         "volatile",      "while"
      ));

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
   */
  @Override
  public boolean needsHighLight(String text) {
    return javaKeywords.contains(text);
  }

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String, sh4j.model.style.SStyle)
   */
  @Override
  public String highlight(String text, SStyle style) {
    return style.formatKeyWord(text);
  }
}

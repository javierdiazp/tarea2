package sh4j.model.highlight;

import sh4j.model.style.SStyle;

/**
 * Define la estructura de las clases que representan
 * los tipos de palabras que pueden ser destacadas.
 * @author Javier
 */
public interface SHighlighter{
  /**
   * Indica que el texto debe ser destacado.
   * @param text parte minimal del codigo, como palabras, simbolos, etc.
   * @return true si text debe ser destacado, false si no.
   */
  public boolean needsHighLight(String text);

  /**
   * Destaca un texto segun un estilo, en codigo HTML.
   * @param text parte minimal del codigo.
   * @param style conjunto de reglas de personalizacion de un texto.
   * @return el codigo HTML del texto con estilo. 
   */
  public String highlight(String text,SStyle style);
}
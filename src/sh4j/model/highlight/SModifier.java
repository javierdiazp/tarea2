package sh4j.model.highlight;

import sh4j.model.style.SStyle;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Describe el conjunto de palabras de tipo Modifier.
 * @author Javier
 */
public class SModifier implements SHighlighter{

  /**
   * Conjunto de modifiers de Java.
   */
  private static final Set<String> javaModifiers = new HashSet<String>(Arrays.asList("private", 
      "protected","public"));

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
   */
  @Override
  public boolean needsHighLight(String text) {
    return javaModifiers.contains(text);
  }

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String, sh4j.model.style.SStyle)
   */
  @Override
  public String highlight(String text, SStyle style) {
    return style.formatModifier(text);
  }

}

package sh4j.model.highlight;

import sh4j.model.style.SStyle;

/**
 * Describe el conjunto de palabras de tipo PseudoVariable.
 * @author Javier
 */
public class SPseudoVariable implements SHighlighter{

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
   */
  @Override
  public boolean needsHighLight(String text) {
    return "super".equals(text) || "this".equals(text);
  }

  /** (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String, sh4j.model.style.SStyle)
   */
  @Override
  public String highlight(String text, SStyle style) {
    return style.formatPseudoVariable(text);
  }


}

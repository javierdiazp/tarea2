package sh4j.parser;

import java.util.*;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import sh4j.model.browser.SClass;

public class SClassParser extends ASTVisitor{
  private List<SClass> classes;

  public SClassParser(){
    classes=new ArrayList<SClass>();
  }
  public boolean visit(TypeDeclaration node) {
    SMethodParser parser=new SMethodParser();
    node.accept(parser);
    classes.add(new SClass(node,parser.methods()));
    return super.visit(node);
  }

  public List<SClass> classes(){
    return classes;
  }

  public static List<SClass> parse(String source){
    ASTParser parser = ASTParser.newParser(AST.JLS8);
    parser.setSource(source.toCharArray());
    parser.setKind(ASTParser.K_COMPILATION_UNIT);
    final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
    SClassParser builder=new SClassParser();
    cu.accept(builder);     
    return builder.classes();
  }

}
